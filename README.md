![Custom Music](About/Preview.png)

# Custom Music RimWorld Mod

As great as RimWorld's music is, sometimes it's time for a change. With
this mod you can listen to your local music library in RimWorld. Just
point it to a directory and it will add the songs to the game's track
list.

For clarity, as it seems to be unclear: this mod is *not* a media
player in RimWorld. It is a loader for external music files to be
played according to the same rules as vanilla music or the songs of any
music pack mod, like p-music.

Your music will be played as situational as you configure it to be.
Want to fight off raiders while listening to DragonForce? You can! Want
to meet fall with some classical piano music? Go right ahead.

## Features

* File Explorer (Open): Conveniently navigate to the directory with
  your music. Click OK to use the current directory, or Cancel to keep
  using the previous one.
* The music directory can have subdirectories that will be searched for
  files, too.
* Click Update to rescan the directory. Adds new songs, removes configs
  for removed songs and keeps the rest.
* Quick access to volume and "battle music" flag
* Configure all settings in the advanced settings (gear column)

## Getting OGG files

In case you don't know how to convert your music to ogg, here is a
simple guide.

1. Download fre:ac[www.freac.org], a free, open source audio converter.
   The zip doesn't require installation. Packages for OSX and Linux are
   offered, too.
1. Unpack/install to a directory of your choice
1. Start the program (freac.exe on Windows, freac on Linux, freac
   application on macOS
1. Add all the files you want to convert to the Joblist (you can add
   single files as well as directories)
1. In Options > General settings… set the Filename pattern to something
   something simple as `<title>` or `<artist> - <title>`. Check "Append
   sequential numbers…"
1. Optionally: Options > Configure selected encoder… Choose an encoding
   mode. VBR is fine if you don't know the difference. Choose a
   quality. Higher value means better quality but larger file. Keep in
   mind that qualty can't actually be increased when converting from
   mp3 or other lossy formats, but the file will still be larger. 6
   should be a good value for most cases.
1. Set the output folder to the directory you want RimWorld to play
   music from
1. Start the conversion (circular play button)

## Words of caution

RimWorld's ogg player is very sensitive to "irregularities". Should you
already have OGG files or have acquired them from another program, make
sure they don't contain a video track. Some encoders put the cover art
in a video track, instead of the ID3 tags. RimWorld will not only not
play these files, but they can even prevent valid songs from loading
properly. So if you have trouble with your songs playing, perhaps try
adding them one by one to see which one breaks the chain.

## Acknowledgements

My thanks go to erdelf, who put me on the right track on where to hook
into the game, as well as WhyIsThat, KeenKrozzy and Brrainz for helping
to optimise the loading procedure. Thanks to ison, who helped getting
things going for A18.

![Harmony](https://raw.githubusercontent.com/pardeike/Harmony/master/HarmonyLogo.png)
