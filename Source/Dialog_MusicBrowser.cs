﻿using Verse;
using UnityEngine;

using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CustomMusic {
	public class Dialog_MusicBrowser : Window {
		protected float margin = 4f;
		protected float lineHeight = 24f;
		protected float buttonHeight = 24f;
		protected float scrollBarWidth = 16f;
		protected static string invalidChars = new string(Path.GetInvalidPathChars());
		protected Color fileTextColor = new Color(0.6f, 0.6f, 0.6f);

		private Vector2 scrollPos = Vector2.zero;
		private string currentPath;
		private List<string> currentDirContent = new List<string>();

		public Dialog_MusicBrowser() {
			this.doCloseX = true;
			this.forcePause = true;
			this.absorbInputAroundWindow = true;
			this.currentPath = CustomMusicCore.settings.musicPath;
		}

		public override void DoWindowContents(Rect inRect) {
			float marginFrac = margin / inRect.height;
			float headerFrac = lineHeight / inRect.height;
			float buttonFrac = (buttonHeight + margin) / (inRect.height - lineHeight - margin);
			Rect header = inRect.TopPart(headerFrac);
			Rect main = inRect.BottomPart(1 - headerFrac - marginFrac).TopPart(1 - buttonFrac);
			Rect buttons = inRect.BottomPart(1 - headerFrac).BottomPart(buttonFrac);
			string labelCurrentDir = "CustomMusic_currentDir".Translate();
			float fracLabel = Mathf.Ceil(Text.CalcSize(labelCurrentDir).x + margin) / inRect.width;
			// Header line
			Text.Anchor = TextAnchor.MiddleLeft;
			Widgets.Label(header.LeftPart(fracLabel), labelCurrentDir);
			Rect pathTextField = header.RightPart(1 - fracLabel);
			// Detect Windows
			if (Environment.OSVersion.Platform < PlatformID.Unix) {
				string driveButton = "CustomMusic_drive".Translate();
				float buttonWidth = Text.CalcSize(driveButton).x + margin * 2;
				if (Widgets.ButtonText(new Rect(pathTextField.x, pathTextField.y, buttonWidth, pathTextField.height), driveButton)) {
					List<FloatMenuOption> driveList = new List<FloatMenuOption>();
					foreach (string drive in Directory.GetLogicalDrives()) {
						driveList.Add(new FloatMenuOption(drive, delegate {
							currentPath = drive;
						}));
					}
					Find.WindowStack.Add(new FloatMenu(driveList));
				}
				pathTextField.x += buttonWidth + margin;
				pathTextField.width -= buttonWidth + margin;
			}
			currentPath = Widgets.TextField(pathTextField, currentPath, int.MaxValue,
												new Regex("[^" + invalidChars + "]*"));
			// Directory view
			Rect directoryView = new Rect(main.x, main.y, main.width, main.height);
			if (Directory.Exists(currentPath)) {
				currentDirContent = Directory.GetFileSystemEntries(currentPath, "*").ToList();
				if (new DirectoryInfo(currentPath).Parent != null)
					currentDirContent.Insert(0, "..");
			}
			Rect viewRect = new Rect(0, 0, directoryView.width - scrollBarWidth, lineHeight * currentDirContent.Count);
			Widgets.BeginScrollView(directoryView, ref scrollPos, viewRect);
			for (int i = 0; i < currentDirContent.Count; i++) {
				string cur = currentDirContent[i];
				FileAttributes attr = File.GetAttributes(cur);
				if (i % 2 == 0)
					Widgets.DrawAltRect(new Rect(0f, lineHeight * i, viewRect.width, lineHeight));
				if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
					if (Widgets.ButtonInvisible(new Rect(0f, lineHeight * i, viewRect.width, lineHeight))) {
						if (cur.Equals("..", StringComparison.Ordinal))
							this.currentPath = Directory.GetParent(currentPath).FullName;
						else
							this.currentPath = cur;
					}
					GUI.color = Color.white;
					Widgets.Label(new Rect(2f, lineHeight * i, viewRect.width - 4f, lineHeight), Path.GetFileName(cur));
				} else {
					GUI.color = this.fileTextColor;
					Widgets.Label(new Rect(2f, lineHeight * i, viewRect.width - 4f, lineHeight), Path.GetFileName(cur));
				}
			}
			Widgets.EndScrollView();
			// Button line
			GUI.color = Color.white;
			WidgetRow buttonRow = new WidgetRow(buttons.width + buttons.x, buttons.height + buttons.y - buttonHeight,
												UIDirection.LeftThenUp, buttons.width, margin);
			if (buttonRow.ButtonText("CustomMusic_browserOk".Translate(), "CustomMusic_browserOk_desc".Translate())) {
				CustomMusicCore.settings.musicPath = this.currentPath;
				CustomMusicCore.ScanMusicPath();
				this.Close(true);
			}
			if (buttonRow.ButtonText("CustomMusic_browserCancel".Translate(),
									 "CustomMusic_browserCancel_desc".Translate()))
				this.Close(false);
			Text.Anchor = TextAnchor.UpperLeft;
		}
	}
}
