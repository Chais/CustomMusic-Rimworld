﻿using Verse;
using RimWorld;
using UnityEngine;

using System.IO;

namespace CustomMusic {
	public class Dialog_AdvancedSettings : Window {
		private readonly CustomSongDef song = null;
		protected float margin = 4f;
		protected float lineHeight = 24f;
		protected float lineSkip = 24f;
		protected bool spring;
		protected bool summer;
		protected bool fall;
		protected bool winter;
		protected bool perm_summer;
		protected bool perm_winter;
		protected int int_commonality;
		protected int int_volume;

		public Dialog_AdvancedSettings(CustomSongDef song) {
			this.doCloseX = true;
			this.doCloseButton = true;
			this.forcePause = true;
			this.absorbInputAroundWindow = true;

			this.song = song;
			this.spring = song.PlaysDuringSpring;
			this.summer = song.PlaysDuringSummer;
			this.fall = song.PlaysDuringFall;
			this.winter = song.PlaysDuringWinter;
			this.perm_summer = song.PlaysDuringPermSummer;
			this.perm_winter = song.PlaysDuringPermWinter;
			if (!(spring || summer || fall || winter || perm_summer || perm_winter))
				this.spring = this.summer = this.fall = this.winter = this.perm_summer = this.perm_winter = true;
		}

		public override void PreOpen() {
			base.PreOpen();
			this.int_commonality = (int) System.Math.Round((this.song.commonality * 100));
			this.int_volume = (int) System.Math.Round((this.song.volume * 100));
		}

		public override void DoWindowContents(Rect inRect) {
			if (this.song == null)
				this.Close(false);
			Rect rect = inRect.AtZero();
			// Header
			string name = Path.GetFileNameWithoutExtension(this.song.clipPath);
			Text.Font = GameFont.Medium;
			Text.Anchor = TextAnchor.MiddleCenter;
			Vector2 nameSize = Text.CalcSize(name);
			Widgets.Label(rect.TopPartPixels(nameSize.y + 2 * margin).ContractedBy(margin), name);
			Text.Anchor = TextAnchor.MiddleLeft;

			Listing_Standard content = new Listing_Standard();
			content.Begin(rect.BottomPartPixels(rect.height - nameSize.y - 2 * margin));
			float content_width = rect.width - 34f;

			Listing_Standard seasons = content.BeginSection(lineHeight * 3);
			seasons.ColumnWidth = content_width / 4f;
			seasons.Label("CustomMusic_allowedSeasons".Translate());
			seasons.CheckboxLabeled(SeasonUtility.LabelCap(Season.Spring), ref spring);
			seasons.CheckboxLabeled(SeasonUtility.LabelCap(Season.Summer), ref summer);
			seasons.NewColumn();
			seasons.Label("");
			seasons.CheckboxLabeled(SeasonUtility.LabelCap(Season.Fall), ref fall);
			seasons.CheckboxLabeled(SeasonUtility.LabelCap(Season.Winter), ref winter);
			seasons.NewColumn();
			seasons.ColumnWidth = content_width / 2f - 8f;
			seasons.Label("");
			seasons.CheckboxLabeled(SeasonUtility.LabelCap(Season.PermanentSummer), ref perm_summer);
			seasons.CheckboxLabeled(SeasonUtility.LabelCap(Season.PermanentWinter), ref perm_winter);
			content.EndSection(seasons);

			content.Gap(8f);

			Listing_Standard daytime = content.BeginSection(lineHeight * 2f);
			daytime.ColumnWidth = content_width / 3f - 3f;
			daytime.Label("CustomMusic_allowedTimeOfDay".Translate());
			if (daytime.RadioButton("Day".Translate(), this.song.allowedTimeOfDay == TimeOfDay.Day))
				this.song.allowedTimeOfDay = TimeOfDay.Day;
			daytime.NewColumn();
			daytime.Label("");
			if (daytime.RadioButton("CustomMusic_Night".Translate(), this.song.allowedTimeOfDay == TimeOfDay.Night))
				this.song.allowedTimeOfDay = TimeOfDay.Night;
			daytime.NewColumn();
			daytime.Label("");
			if (daytime.RadioButton("CustomMusic_Any".Translate(), this.song.allowedTimeOfDay == TimeOfDay.Any))
				this.song.allowedTimeOfDay = TimeOfDay.Any;
			daytime.EndSection(daytime);

			content.Gap(8f);

			Listing_Standard tense = content.BeginSection(lineHeight);
			tense.CheckboxLabeled("CustomMusic_tense".Translate(), ref this.song.tense);
			content.EndSection(tense);

			content.Gap(8f);

			Listing_Standard commonality = content.BeginSection(lineHeight);
			commonality.ColumnWidth = content_width / 2f;
			commonality.Label("CustomMusic_commonality".Translate());
			commonality.NewColumn();
			string str_commonality = this.int_commonality.ToString();
			commonality.IntEntry(ref this.int_commonality, ref str_commonality);
			this.int_commonality = System.Math.Max(0, System.Math.Min(this.int_commonality, 100));
			content.EndSection(commonality);

			content.Gap(8f);

			Listing_Standard play_on_map = content.BeginSection(lineHeight);
			play_on_map.CheckboxLabeled("CustomMusic_playOnMap".Translate(), ref this.song.playOnMap);
			content.EndSection(play_on_map);

			content.Gap(8f);

			Listing_Standard volume = content.BeginSection(lineHeight);
			volume.ColumnWidth = content_width / 2f;
			volume.Label("CustomMusic_volume".Translate());
			volume.NewColumn();
			string str_volume = this.int_volume.ToString();
			volume.IntEntry(ref this.int_volume, ref str_volume);
			volume.ColumnWidth = content_width / 2f;
			this.int_volume = System.Math.Max(0, System.Math.Min(this.int_volume, 100));
			content.EndSection(volume);
			content.End();

			Text.Anchor = TextAnchor.UpperLeft;
		}

		public override void PostClose() {
			this.song.SetAllowedSeasons(this.spring, this.summer, this.fall, this.winter, this.perm_summer, this.perm_winter);
			this.song.commonality = this.int_commonality / 100f;
			this.song.volume = this.int_volume / 100f;
			base.PostClose();
		}
	}
}
